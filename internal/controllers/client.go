package controllers

import (
	"bytes"
	"io/ioutil"
	"net/http"
)

type ClientController struct {
	client *http.Client
}

func NewClientController(client *http.Client) *ClientController {
	return &ClientController{client: client}
}

func (c *ClientController) Send(url string, body []byte) ([]byte, error) {
	req, err := http.NewRequest("POST", url, bytes.NewReader(body))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = resp.Body.Close()
	}()

	out, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return out, nil
}
