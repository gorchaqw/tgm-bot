package controller

import (
	"fmt"

	"tgm-bot/internal/events"
)

type GetMeEvent struct {
	URL string
}

func NewGetMeEvent(baseURL string) *GetMeEvent {
	return &GetMeEvent{URL: fmt.Sprintf("%s/getMe", baseURL)}
}

const GetMeEventName events.EventName = "GetMeEvent"

func (e *GetMeEvent) GetType() events.EventType {
	return events.EventTypeController
}

func (e *GetMeEvent) GetName() events.EventName {
	return GetMeEventName
}
