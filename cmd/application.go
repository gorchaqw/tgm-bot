package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"

	"net/http"

	"tgm-bot/internal/observer"
)

type App struct {
	Fiber         *fiber.App
	Config        *Config
	Logger        *logrus.Logger
	EventNotifier *observer.EventNotifier
	HTTPClient    *http.Client
}
