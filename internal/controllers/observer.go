package controllers

import (
	"encoding/json"

	"tgm-bot/internal/controllers/structs"
	"tgm-bot/internal/events"
	"tgm-bot/internal/events/controller"
	"tgm-bot/internal/observer"

	"github.com/sirupsen/logrus"
)

type Observer struct {
	token            string
	baseURL          string
	clientController *ClientController
	logger           *logrus.Logger
	notifier         *observer.EventNotifier
	offset           int
}

func NewControllerObserver(
	token, url string,
	clientController *ClientController,
	logger *logrus.Logger,
	notifier *observer.EventNotifier,
) *Observer {
	return &Observer{
		token:            token,
		baseURL:          url,
		clientController: clientController,
		logger:           logger,
		notifier:         notifier,
	}
}

func (o *Observer) OnNotify(e events.Event) {
	switch e.GetType() {
	case events.EventTypeController:
		switch e.GetName() {
		case controller.GetMeEventName:
			o.procGetMeEvent(e.(*controller.GetMeEvent))
		case controller.SendMessageName:
			o.procSendMessageEvent(e.(*controller.SendMessage))
		case controller.GetUpdatesName:
			o.procGetUpdatesEvent(e.(*controller.GetUpdatesEvent))
		default:
			o.logger.Debugf("invalid event name: %s", e.GetType())
		}
	default:
		o.logger.Debugf("invalid event type: %s", e.GetType())
	}
}

func (o *Observer) procSendMessageEvent(e *controller.SendMessage) {
	body, err := json.Marshal(e)
	if err != nil {
		o.logger.Error(err)
		return
	}

	out, err := o.clientController.Send(e.URL, body)
	if err != nil {
		o.logger.Error(err)
		return
	}

	o.logger.Debug(string(out))
}

func (o *Observer) procGetMeEvent(e *controller.GetMeEvent) {
	out, err := o.clientController.Send(e.URL, nil)
	if err != nil {
		o.logger.Error(err)
		return
	}

	o.logger.Debug(out)
}

func (o *Observer) procGetUpdatesEvent(e *controller.GetUpdatesEvent) {
	e.Offset = o.offset

	body, err := json.Marshal(e)
	if err != nil {
		o.logger.Error(err)
		return
	}

	out, err := o.clientController.Send(e.URL, body)
	if err != nil {
		o.logger.Error(err)
		return
	}

	var getUpdates structs.GetUpdates
	if err := json.Unmarshal(out, &getUpdates); err != nil {
		o.logger.Error(err)
		return
	}

	for i := range getUpdates.Result {
		if o.offset == 0 {
			o.offset = getUpdates.Result[i].UpdateID
		}

		if getUpdates.Result[i].UpdateID != o.offset {
			o.notifier.Notify(controller.NewSendMessageEvent(
				o.baseURL,
				getUpdates.Result[i].Message.Chat.ID,
				getUpdates.Result[i].Message.Text,
				getUpdates.Result[i].Message.MessageID,
			))

			o.offset = getUpdates.Result[i].UpdateID

			o.logger.Debug(string(out))
		}
	}
}
