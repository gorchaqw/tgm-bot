package main

import (
	"fmt"
)

func main() {
	var app App

	app.initLogger()

	if err := app.loadConfig(); err != nil {
		panic(err)
	}

	app.initHTTPClient()
	app.initEventNotifier()
	app.initFiber()
	app.registerHTTPEndpoints()

	if err := app.Fiber.Listen(fmt.Sprintf(":%s", app.Config.AppPort)); err != nil {
		panic(err)
	}
}
