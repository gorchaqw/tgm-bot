package controller

import (
	"fmt"

	"tgm-bot/internal/events"
)

type GetUpdatesEvent struct {
	URL            string   `json:"-"`
	Offset         int      `json:"offset"`
	Limit          int      `json:"limit"`
	Timeout        int      `json:"timeout"`
	AllowedUpdates []string `json:"allowed_updates"`
}

func NewGetUpdatesEvent(baseURL string, allowedUpdates []string) *GetUpdatesEvent {
	return &GetUpdatesEvent{
		URL:            fmt.Sprintf("%s/getUpdates", baseURL),
		Limit:          100,
		AllowedUpdates: allowedUpdates}
}

const GetUpdatesName events.EventName = "GetUpdatesEvent"

func (e *GetUpdatesEvent) GetType() events.EventType {
	return events.EventTypeController
}

func (e *GetUpdatesEvent) GetName() events.EventName {
	return GetUpdatesName
}
