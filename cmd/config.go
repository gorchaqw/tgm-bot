package main

import (
	"github.com/joho/godotenv"

	"errors"
	"fmt"
	"os"
)

type Config struct {
	AppPort    string
	AppName    string
	TgmToken   string
	TgmBaseURL string
}

var ErrEnvNotFound = errors.New("err env not found")

func (a *App) loadConfig() error {
	var cfg Config

	err := godotenv.Load()
	if err != nil {
		return err
	}

	if cfg.AppPort, err = cfg.set("APP_PORT"); err != nil {
		return err
	}

	if cfg.AppName, err = cfg.set("APP_NAME"); err != nil {
		return err
	}

	if cfg.TgmToken, err = cfg.set("TGM_TOKEN"); err != nil {
		return err
	}

	cfg.TgmBaseURL = fmt.Sprintf("https://api.telegram.org/bot%s", cfg.TgmToken)

	a.Config = &cfg

	return nil
}

func (c *Config) set(key string) (string, error) {
	if os.Getenv(key) == "" {
		return "", ErrEnvNotFound
	}

	return os.Getenv(key), nil
}
