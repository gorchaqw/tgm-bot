package main

import "tgm-bot/internal/api/http"

func (a *App) registerHTTPEndpoints() {
	http.RegisterHTTPEndpoints(a.Fiber, a.Logger, a.EventNotifier, a.Config.TgmBaseURL)
}
