package controller

import (
	"fmt"

	"tgm-bot/internal/events"
)

type SendMessage struct {
	URL              string `json:"-"`
	ChatID           int    `json:"chat_id"`
	Text             string `json:"text"`
	ReplyToMessageID int    `json:"reply_to_message_id"`
}

func NewSendMessageEvent(baseURL string, chatID int, text string, replyToMessageID int) *SendMessage {
	return &SendMessage{
		URL:              fmt.Sprintf("%s/sendMessage", baseURL),
		ChatID:           chatID,
		Text:             text,
		ReplyToMessageID: replyToMessageID,
	}
}

const SendMessageName events.EventName = "SendMessage"

func (e *SendMessage) GetType() events.EventType {
	return events.EventTypeController
}

func (e *SendMessage) GetName() events.EventName {
	return SendMessageName
}
