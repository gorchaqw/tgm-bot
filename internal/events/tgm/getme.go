package tgm

import "tgm-bot/internal/events"

type GetMeEvent struct{}

const GetMeEventName events.EventName = "GetMeEvent"

func (e *GetMeEvent) GetType() events.EventType {
	return events.EventTypeTGM
}

func (e *GetMeEvent) GetName() events.EventName {
	return GetMeEventName
}
