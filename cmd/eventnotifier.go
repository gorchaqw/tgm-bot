package main

import (
	"tgm-bot/internal/controllers"
	"tgm-bot/internal/observer"
)

func (a *App) initEventNotifier() {
	a.EventNotifier = observer.NewEventNotifier()
	controllerClient := controllers.NewClientController(a.HTTPClient)

	observerClient := controllers.NewControllerObserver(
		a.Config.TgmToken,
		a.Config.TgmBaseURL,
		controllerClient,
		a.Logger,
		a.EventNotifier,
	)

	a.EventNotifier.Register(observerClient)
}
