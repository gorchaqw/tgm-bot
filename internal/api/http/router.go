package http

import (
	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"

	"tgm-bot/internal/observer"
)

func RegisterHTTPEndpoints(f *fiber.App, l *logrus.Logger, e *observer.EventNotifier, b string) {
	h := NewHandler(f, l, e, b)
	router := f.Group("api")
	router.Get("/", h.HelloWorld)
	router.Get("/listen", h.Listen)
	router.Get("/stop", h.Stop)
}
