package http

import (
	"tgm-bot/internal/events/controller"
	"tgm-bot/internal/observer"

	"github.com/gofiber/fiber/v2"
	"github.com/sirupsen/logrus"

	"time"
)

type Handler struct {
	fiber    *fiber.App
	logger   *logrus.Logger
	notifier *observer.EventNotifier
	baseURL  string
	ticker   *time.Ticker
	offset   int
	done     chan bool
}

const TickerUpdate = 500

func NewHandler(f *fiber.App, l *logrus.Logger, e *observer.EventNotifier, baseURL string) *Handler {
	return &Handler{
		fiber:    f,
		logger:   l,
		notifier: e,
		baseURL:  baseURL,
		ticker:   time.NewTicker(TickerUpdate * time.Millisecond),
		offset:   0,
		done:     make(chan bool),
	}
}

func (h *Handler) Stop(c *fiber.Ctx) error {
	body := struct {
		Status bool `json:"status"`
	}{
		Status: true,
	}

	h.done <- true

	if err := c.JSON(body); err != nil {
		return err
	}

	return nil
}
func (h *Handler) Listen(c *fiber.Ctx) error {
	body := struct {
		Status bool `json:"status"`
	}{
		Status: true,
	}

	go func() {
		for {
			select {
			case <-h.done:
				return
			case <-h.ticker.C:
				h.notifier.Notify(controller.NewGetUpdatesEvent(h.baseURL, []string{}))
				h.offset++
			}
		}
	}()

	if err := c.JSON(body); err != nil {
		return err
	}

	return nil
}

func (h *Handler) HelloWorld(c *fiber.Ctx) error {
	body := struct {
		Status bool `json:"status"`
	}{
		Status: true,
	}

	h.notifier.Notify(controller.NewGetMeEvent(h.baseURL))
	h.notifier.Notify(controller.NewGetUpdatesEvent(h.baseURL, []string{}))

	if err := c.JSON(body); err != nil {
		return err
	}

	return nil
}
