package events

type EventType string
type EventName string

const EventTypeTGM EventType = "EventTypeTGM"
const EventTypeController EventType = "EventTypeController"

type Event interface {
	GetType() EventType
	GetName() EventName
}
