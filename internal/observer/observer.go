package observer

import (
	"tgm-bot/internal/events"
)

type Observer interface {
	OnNotify(events.Event)
}

type Notifier interface {
	Register(Observer)
	DeRegister(Observer)
	Notify(events.Event)
}

type EventObserver struct {
	ID int
}

type EventNotifier struct {
	Observers map[Observer]struct{}
}

func NewEventNotifier() *EventNotifier {
	return &EventNotifier{
		Observers: map[Observer]struct{}{},
	}
}

func (n *EventNotifier) Register(l Observer) {
	n.Observers[l] = struct{}{}
}

func (n *EventNotifier) DeRegister(l Observer) {
	delete(n.Observers, l)
}

func (n *EventNotifier) Notify(e events.Event) {
	for o := range n.Observers {
		o.OnNotify(e)
	}
}
